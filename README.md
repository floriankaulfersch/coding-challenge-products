## CCProducts - A Webapp for Products

### Running the app

#### Set necessary properties
* In `src/main/resources/application.properties` set `storage.path.root` to where you want your images to be stored.
* Copy folder `src/main/resources/static/img/firstproduct` to wherever you pointed `storage.path.root` to
* In `src/main/webapp/products-fe/` duplicate and rename`.env.dev` to `.env.local` and insert your fixer api key


#### Run the backend Spring Boot application 
Follow the steps detailed here:
https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html#using-boot-running-your-application

#### Run the react frontend 
    
 With npm installed:

 * Change paths to src/main/webapp/products-fe
 * Run `npm install`
 * Run `npm start`
 * navigate to `http://localhost:3000`

### Swagger api-docs

Can be viewed under http://localhost:8080/v2/api-docs

### Swagger UI

Can be viewed under http://localhost:8080/swagger-ui.html