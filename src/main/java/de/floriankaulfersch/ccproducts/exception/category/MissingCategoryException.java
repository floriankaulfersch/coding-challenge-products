package de.floriankaulfersch.ccproducts.exception.category;

public class MissingCategoryException extends RuntimeException {

    public MissingCategoryException(){
        super("Category field is missing or invalid");
    }

    public MissingCategoryException(String message) {
        super(message);
    }

    public MissingCategoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
