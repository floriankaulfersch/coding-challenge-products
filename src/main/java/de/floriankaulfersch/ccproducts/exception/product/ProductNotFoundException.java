package de.floriankaulfersch.ccproducts.exception.product;

import de.floriankaulfersch.ccproducts.exception.NotFoundException;

public class ProductNotFoundException extends NotFoundException {

    public ProductNotFoundException(Integer id) {
        super(String.format("No product with id %s fround", id));
    }
}
