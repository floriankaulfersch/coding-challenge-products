package de.floriankaulfersch.ccproducts.model.product;

import de.floriankaulfersch.ccproducts.model.category.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    private String name;

    @Column(length = 1000)
    private String description;

    @NotNull
    @OneToOne
    private Image baseImagePath;

    @OneToMany
    private List<Image> additionalImagePaths;

    @ElementCollection
    @CollectionTable(name="product_categories", joinColumns = @JoinColumn(name="product_id"))
    @Column(name="category_id")
    private Set<Integer> categoryIds;

    @NotNull
    private Double price;

    @Override
    public boolean equals(Object o){
        if (!(o instanceof Product)) return false;
        if (this.id == null && ((Product)o).getId() == null) return true;

        return id.equals(((Product) o).getId());
    }
}
