package de.floriankaulfersch.ccproducts.model.product;

import de.floriankaulfersch.ccproducts.model.category.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {

    private Product product;
    private Category category;
    private String baseImage64;
    private List<String> additionalImages64;
}
