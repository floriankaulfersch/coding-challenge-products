package de.floriankaulfersch.ccproducts.model.category;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CategoryTreeNode {

    private Category category;
    private List<CategoryTreeNode> subCategories;

    public CategoryTreeNode(Category category){
        this.category = category;
    }
}
