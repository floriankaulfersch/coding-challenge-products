package de.floriankaulfersch.ccproducts.infrastructure.controller;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import de.floriankaulfersch.ccproducts.exception.product.ProductNotFoundException;
import de.floriankaulfersch.ccproducts.infrastructure.facades.ProductFacade;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.ProductRepository;
import de.floriankaulfersch.ccproducts.model.product.Product;
import de.floriankaulfersch.ccproducts.model.product.ProductRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductRepository productRepository;

    private final ProductFacade productFacade;

    @GetMapping
    public Iterable<Product> getProducts(@RequestParam(required = false) Integer categoryId){
        if (categoryId == null){
            return productRepository.findAll();
        } else {
            return productRepository.findByCategoryIdsContains(categoryId);
        }
    }

    @GetMapping("/{productId}")
    public Product getProduct(@PathVariable Integer productId){

        Optional<Product> product = productRepository.findById(productId);
        if (product.isPresent()){
            return product.get();
        }
        throw new ProductNotFoundException(productId);
    }

    @PostMapping
    public Product addProduct(@RequestBody ProductRequest productRequest) throws Base64DecodingException {
        return productFacade.addProduct(productRequest);
    }

    @PutMapping("/{productId}")
    public Product updateProduct(@PathVariable Integer productId, @RequestBody ProductRequest productRequest) throws Base64DecodingException {
        if (productId.equals(productRequest.getProduct().getId()) && productRepository.existsById(productId)){
            return productFacade.addProduct(productRequest);
        }
        throw new ProductNotFoundException(productId);
    }

    @DeleteMapping("/{productId}")
    public void deleteProduct(@PathVariable Integer productId) {
        productFacade.deleteProduct(productId);
    }
}
