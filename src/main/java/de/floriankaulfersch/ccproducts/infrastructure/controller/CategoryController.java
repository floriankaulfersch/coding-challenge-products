package de.floriankaulfersch.ccproducts.infrastructure.controller;

import de.floriankaulfersch.ccproducts.infrastructure.facades.CategoryFacade;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.CategoryRepository;
import de.floriankaulfersch.ccproducts.model.category.Category;
import de.floriankaulfersch.ccproducts.model.category.CategoryTreeNode;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryRepository categoryRepository;
    private final CategoryFacade categoryFacade;

    @GetMapping("/flat")
    public Iterable<Category> getAllCategories() {return categoryRepository.findAll();}

    @GetMapping
    public List<CategoryTreeNode> getAllCategoriesMap(){
        return categoryFacade.getCategoryTree();
    }

    @PostMapping
    public Category addCategory(@RequestBody Category category){
        return categoryRepository.save(category);
    }

    @DeleteMapping("/{categoryId}")
    public void deleteCategory(@PathVariable Integer categoryId){
        categoryRepository.deleteById(categoryId);
    }

    @PutMapping("/{categoryId}")
    public Category updateCategory(@PathVariable Integer categoryId){
        //TODO
        return null;
    }
}
