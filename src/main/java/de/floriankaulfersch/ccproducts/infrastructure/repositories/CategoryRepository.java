package de.floriankaulfersch.ccproducts.infrastructure.repositories;

import de.floriankaulfersch.ccproducts.model.category.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
}
