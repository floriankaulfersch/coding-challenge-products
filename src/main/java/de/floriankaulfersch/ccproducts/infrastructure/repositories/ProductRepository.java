package de.floriankaulfersch.ccproducts.infrastructure.repositories;

import de.floriankaulfersch.ccproducts.model.product.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {

    Iterable<Product> findByCategoryIdsContains(Integer categoryId);
}
