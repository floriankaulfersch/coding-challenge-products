package de.floriankaulfersch.ccproducts.infrastructure.repositories;

import de.floriankaulfersch.ccproducts.model.product.Image;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Integer> {
}
