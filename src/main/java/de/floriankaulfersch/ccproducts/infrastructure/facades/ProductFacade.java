package de.floriankaulfersch.ccproducts.infrastructure.facades;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import de.floriankaulfersch.ccproducts.exception.category.MissingCategoryException;
import de.floriankaulfersch.ccproducts.infrastructure.files.ClassPathImageStorageService;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.CategoryRepository;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.ImageRepository;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.ProductRepository;
import de.floriankaulfersch.ccproducts.model.category.Category;
import de.floriankaulfersch.ccproducts.model.product.Image;
import de.floriankaulfersch.ccproducts.model.product.Product;
import de.floriankaulfersch.ccproducts.model.product.ProductRequest;
import de.floriankaulfersch.ccproducts.util.ImageData;
import de.floriankaulfersch.ccproducts.util.ImageDecoder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductFacade {

    private final ProductRepository productRepository;
    private final ClassPathImageStorageService classPathImageStorageService;

    private final ImageRepository imageRepository;
    private final CategoryRepository categoryRepository;

    private int imageIndex;

    public Product addProduct(ProductRequest productRequest) throws Base64DecodingException {

        checkData(productRequest);
        Optional<Category> category = categoryRepository.findById(productRequest.getCategory().getId());

        if (!category.isPresent()){
            throw new MissingCategoryException();
        }

        Set<Integer> categoryIds = getCategoryIds(category.get());
        productRequest.getProduct().setCategoryIds(categoryIds);

        handleAddProductImages(productRequest);

        return productRepository.save(productRequest.getProduct());
    }

    private void handleAddProductImages(ProductRequest productRequest) throws Base64DecodingException {
        Product product = productRequest.getProduct();

        String productImageDirectory = getProductImageDirectory(product);

        resetImageIndex();

        ImageData baseImageData = ImageDecoder.decodeImage(productRequest.getBaseImage64());

        List<ImageData> additionalImageData = productRequest.getAdditionalImages64().stream()
                .map(this::getImageDataFromBase64String)
                .collect(Collectors.toList());

        Image baseImage = imageRepository.save(
                new Image(null, classPathImageStorageService.store(baseImageData.getImage(),
                        productImageDirectory, "base", baseImageData.getExtension())
                )
        );
        List<Image> additionalImages = additionalImageData.stream()
                .map(data -> imageRepository.save(
                        new Image(null, classPathImageStorageService.store(data.getImage(),
                                productImageDirectory, "additional_"+ getAndIncrementImageIndex(), data.getExtension())
                        )
                ))
                .collect(Collectors.toList());

        product.setBaseImagePath(baseImage);
        product.setAdditionalImagePaths(additionalImages);
    }

    private String getProductImageDirectory(Product product) {
        return StringUtils.trimAllWhitespace(product.getName()).toLowerCase();
    }

    private ImageData getImageDataFromBase64String(String base64String){
        try{
            return ImageDecoder.decodeImage(base64String);
        } catch (Base64DecodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void checkData(ProductRequest productRequest) {

        if (productRequest.getCategory() == null){
            throw new MissingCategoryException();
        }
    }



    private int getAndIncrementImageIndex() {
        return imageIndex++;
    }

    private void resetImageIndex() {
        this.imageIndex = 0;
    }

    public void deleteProduct(Integer productId) {
        Optional<Product> product = productRepository.findById(productId);
        if (product.isPresent()){
            productRepository.deleteById(productId);
            purgeProductImageFolder(product.get());
        }
    }

    private void purgeProductImageFolder(Product product) {
        String folderName = getProductImageDirectory(product);
        classPathImageStorageService.delete(folderName);
    }

    public Set<Integer> getCategoryIds(Category category){
        Set<Integer> categoryIds = new HashSet<>();
        categoryIds.add(category.getId());

        Category parent = category.getParent();
        while (parent != null){
            categoryIds.add(parent.getId());
            parent = parent.getParent();
        }

        return categoryIds;
    }
}
