package de.floriankaulfersch.ccproducts.infrastructure.facades;

import de.floriankaulfersch.ccproducts.infrastructure.repositories.CategoryRepository;
import de.floriankaulfersch.ccproducts.model.category.Category;
import de.floriankaulfersch.ccproducts.model.category.CategoryTreeNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Service
public class CategoryFacade {

    private final CategoryRepository categoryRepository;

    /**
     * Gets all categories from the database and returns them as a nested list of main categories
     * @return A List contain
     */
    public List<CategoryTreeNode> getCategoryTree(){
        Iterable<Category> categories = categoryRepository.findAll();

        // Map categories to CategoryTreeNode and store in map
        Map<Integer, CategoryTreeNode> categoryMap = StreamSupport.stream(categories.spliterator(), false)
                .collect(Collectors.toMap(Category::getId, CategoryTreeNode::new));

        Map<Integer, List<CategoryTreeNode>> parentList = categoryMap.values().stream()
                .filter(c->c.getCategory().getParent()!=null)
                .collect(
                        Collectors.toMap(
                                c->c.getCategory().getParent().getId(),
                                c->new ArrayList<>(Arrays.asList(c)),
                                (c1, c2) -> {
                                    c1.addAll(c2);
                                    return c1;
                                }
                        )
                );
        parentList.forEach((parentId, categoryList) -> categoryMap.get(parentId).setSubCategories(categoryList));

        return categoryMap.values().stream()
                .filter(treeNode -> treeNode.getCategory().getParent() == null)
                .collect(Collectors.toList());

    }

}
