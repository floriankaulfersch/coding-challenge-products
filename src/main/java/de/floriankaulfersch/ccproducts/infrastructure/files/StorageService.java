package de.floriankaulfersch.ccproducts.infrastructure.files;

public interface StorageService<T> {

    String store(T file, String fileName, String extension);

    String store(T file, String path, String fileName, String extension);

    void delete(String filePath);
}
