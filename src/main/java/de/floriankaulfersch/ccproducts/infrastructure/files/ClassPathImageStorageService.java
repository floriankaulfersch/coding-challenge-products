package de.floriankaulfersch.ccproducts.infrastructure.files;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ClassPathImageStorageService implements StorageService<BufferedImage> {

    private final Path rootLocation;

    public ClassPathImageStorageService(@Value("${storage.path.root}") String rootLocation) {
        this.rootLocation = Paths.get(rootLocation);
    }

    @Override
    public String store(BufferedImage image, String fileName, String extension) {
        return store(image, "", fileName, extension);
    }

    @Override
    public String store(BufferedImage image, String path, String fileName, String extension) {
        String pathAndFileName = rootLocation.toString() + "/" + path + "/" + fileName + "." + extension;
        storeImage(image, pathAndFileName, extension);
        return path + "/" + fileName + "." + extension;
    }

    private String storeImage(BufferedImage image, String pathAndFileName, String extension){
        try {
            System.out.println(System.getProperty("user.dir"));
            System.out.println(pathAndFileName);


            File imageFile = new File(pathAndFileName);
            imageFile.getParentFile().mkdirs();

            ImageIO.write(image, extension, imageFile);
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + pathAndFileName, e);
        }

        return pathAndFileName;
    }

    @Override
    public void delete(String filePath) {
        File existingFile = new File(rootLocation.toString() + "/" + filePath);
        if (existingFile.exists()){
            FileSystemUtils.deleteRecursively(existingFile);
        }
    }
}
