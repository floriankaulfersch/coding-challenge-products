package de.floriankaulfersch.ccproducts.application.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class FrontendResourcesConfiguration implements WebMvcConfigurer {

    private final String rootPath;

    public FrontendResourcesConfiguration(@Value("${storage.path.root}") String rootPath){ this.rootPath = rootPath;}

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/img/**")
                .addResourceLocations("file:" + rootPath);
    }
}
