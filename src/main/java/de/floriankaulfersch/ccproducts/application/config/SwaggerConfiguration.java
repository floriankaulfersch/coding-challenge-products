package de.floriankaulfersch.ccproducts.application.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;
import java.util.Collections;

/**
 * Configures automatic generation of the Swagger documentation and display via Swagger UI
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    Docket ccProductsApiDocket(ServletContext servletContext) {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .produces(Collections.singleton("application/json"))
                .apiInfo(apiInfo())
                .select()
                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "ccproducts",
                "Coding Challenge Product Application",
                "0.0.1",
                null,
                new Contact("Florian Kaulfersch","","floriankaulfersch@gmail.com"),
                null,
                null,
                Collections.emptyList());
    }
}
