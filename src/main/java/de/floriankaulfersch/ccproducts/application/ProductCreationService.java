package de.floriankaulfersch.ccproducts.application;

import de.floriankaulfersch.ccproducts.infrastructure.facades.ProductFacade;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.CategoryRepository;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.ImageRepository;
import de.floriankaulfersch.ccproducts.infrastructure.repositories.ProductRepository;
import de.floriankaulfersch.ccproducts.model.category.Category;
import de.floriankaulfersch.ccproducts.model.product.Image;
import de.floriankaulfersch.ccproducts.model.product.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class ProductCreationService {

    private final ProductRepository productRepository;
    private final ProductFacade productFacade;
    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;


    @PostConstruct
    void initDummyData(){

        Category category = Category.builder()
                .name("Products")
                .build();
        categoryRepository.save(category);

        Category subCategory1 = Category.builder()
                .name("First Products")
                .parent(category)
                .build();
        subCategory1 = categoryRepository.save(subCategory1);

        Category subCategory2 = Category.builder()
                .name("Second Products-1")
                .parent(subCategory1)
                .build();
        subCategory2 = categoryRepository.save(subCategory2);

        Category subCategory3 = Category.builder()
                .name("Second Products-2")
                .parent(subCategory1)
                .build();
        subCategory3 = categoryRepository.save(subCategory3);

        Category other = Category.builder()
                .name("Other things")
                .build();
        other = categoryRepository.save(other);

        Category other1 = Category.builder()
                .name("Things to buy")
                .parent(other)
                .build();
        other1 = categoryRepository.save(other1);

        Category other2 = Category.builder()
                .name("Things to see")
                .parent(other1)
                .build();
        categoryRepository.save(other2);

        List<Category> categoryList = new ArrayList<>(Arrays.asList(
                category, subCategory1, subCategory2, subCategory3, other, other1, other2
        ));
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            Image blueImage = new Image(null, "firstproduct/blue_dummy.png");
            Image redImage = new Image(null, "firstproduct/red_dummy.png");

            blueImage = imageRepository.save(blueImage);
            redImage = imageRepository.save(redImage);

            Product product = Product.builder()
                    .name(String.format("My first product %d", i))
                    .description("This was the very first product I created. To have a longer looking text, it is always good to include " +
                            "a Lorem ipsum dolor sit amet, consetetur sadipscing elitr, " +
                            "sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. " +
                            "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, " +
                            "no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, " +
                            "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut " +
                            "labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et " +
                            "justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea " +
                            "takimata sanctus est Lorem ipsum dolor sit amet.")
                    .price(99.9)
                    .baseImagePath(random.nextBoolean() ? redImage : blueImage)
                    .additionalImagePaths(Arrays.asList(blueImage, redImage))
                    .categoryIds(productFacade.getCategoryIds(categoryList.get(random.nextInt(categoryList.size()))))
                    .build();

            productRepository.save(product);

        }


    }
}
