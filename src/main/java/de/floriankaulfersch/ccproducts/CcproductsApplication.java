package de.floriankaulfersch.ccproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcproductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CcproductsApplication.class, args);
	}

}
