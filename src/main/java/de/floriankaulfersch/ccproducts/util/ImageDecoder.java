package de.floriankaulfersch.ccproducts.util;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

public class ImageDecoder {

    public static ImageData decodeImage(String base64String) throws Base64DecodingException {
        String[] dataParts = base64String.split(",");
        if (dataParts.length == 1){
            return new ImageData(null, decodeImagePart(dataParts[0]));
        } else if (dataParts.length == 2){
            String extension = dataParts[0].split("/")[1].split(";")[0];
            return new ImageData(extension, decodeImagePart(dataParts[1]));
        }
        throw new Base64DecodingException(String.format("%s is not a valid base64 image string.", base64String));
    }

    private static BufferedImage decodeImagePart(String base64String) throws Base64DecodingException {
        BufferedImage image;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(base64String);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Base64DecodingException(String.format("The decoding process of the Base64 Image String failed. String: %s", base64String));
        }
        return image;
    }
}
