package de.floriankaulfersch.ccproducts.util;

import java.awt.image.BufferedImage;

public class ImageData{
    private String extension;
    private BufferedImage image;

    ImageData(String extension, BufferedImage image){
        this.extension = extension;
        this.image = image;
    }

    public String getExtension() {
        return extension;
    }

    public BufferedImage getImage() {
        return image;
    }
}
