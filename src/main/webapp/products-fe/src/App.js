import React from 'react';
import './App.scss';
import Header from "./Header/Header";
import ProductsFrontendApi from "./API/ProductsFrontendApi";
import ProductOverview from "./ProductOverview/ProductOverview";
import Footer from "./Footer/Footer";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import CategoryOverview from "./Category/CategoryOverview";
import CategoryCreation from "./Category/CategoryCreation";
import ProductDetails from "./Product/ProductDetails";
import FixerApi from "./API/FixerApi";
import ProductCreationAndUpdate from "./Product/ProductCreationAndUpdate";
import {library} from '@fortawesome/fontawesome-svg-core'
import {faPlusSquare} from '@fortawesome/free-solid-svg-icons'

library.add(faPlusSquare);

class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      categories: [],
      categoriesFlat: [],
      products: [],
      fixerData: {
        rates: {
          'EUR': 1
        }
      }
    }
  }

  componentDidMount() {
    this.loadData();

    FixerApi.getExchangeRates()
      .then(data => {
        console.log(data);
        this.setState({fixerData: data.data});
      })
  }

  loadData = () => {
    Promise.all([
        ProductsFrontendApi.getAllCategories(),
        ProductsFrontendApi.getAllProducts(),
        ProductsFrontendApi.getAllCategoriesFlat()
      ])
      .then((data) => {
        this.setState({
          categories: data[0].data,
          products: data[1].data,
          categoriesFlat: data[2].data
        })
      })
      .catch(error => console.error(error));
  };

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <Header
              categories={this.state.categories}
            />
            <div className="main">
              <Switch>
                <Route exact path={["/", "/products", "/categories/:categoryId/products"]} render={(props) =>
                  <ProductOverview {...props} products={this.state.products} categories={this.state.categories}/>}
                />
                <Route exact path="/categories" component={CategoryOverview}/>
                <Route exact path="/categories/add" component={CategoryCreation}/>
                <Route exact path={["/products/add", "/products/edit/:productId"]} render={(props) =>
                  <ProductCreationAndUpdate
                    {...props}
                    reloadData={this.loadData}
                    categories={this.state.categories}
                    categoriesFlat={this.state.categoriesFlat}
                    rates={this.state.fixerData.rates}
                  />
                }
                />
                <Route exact path="/products/:productId" render={(props) =>
                  <ProductDetails
                    {...props}
                    categories={this.state.categories}
                    rates={this.state.fixerData.rates}
                    reloadData={this.loadData}
                  />}
                />
              </Switch>

            </div>
            <Footer />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
