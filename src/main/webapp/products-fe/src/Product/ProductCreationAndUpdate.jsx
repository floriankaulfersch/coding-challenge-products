import React, {Component} from 'react'
import ProductsFrontendApi from "../API/ProductsFrontendApi";
import {Button, Col, Form} from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import FileBase64 from 'react-file-base64';
import './ProductCreationAndUpdate.scss'

export default class ProductCreationAndUpdate extends Component {

  constructor(props) {
    super(props);
    this.state = {

      id: null,
      name: "",
      description: "",
      price: "",

      currency: "EUR",
      category: {
        id: ""
      },
      categoryIds: [],
      submitting: false
    }
  }

  componentDidMount() {
    if (this.props.match.params.productId){
      Promise.all([
          ProductsFrontendApi.getProduct(this.props.match.params.productId),
          ProductsFrontendApi.getAllCategoriesFlat()

        ])
        .then(data => {
          this.setState({
            ...data[0].data,
            categoriesFlat: data[1].data
          }, () => {
            const category = this.getLowestCategory(this.state.categoryIds);
            this.handleCategoryChanged(category)
          })
        })
        .catch(error => console.error(error));
    }
  }

  convertToBaseCurrency = (callBack) => {
    if (this.state.currency !== "EUR") {
      const rate = this.props.rates[this.state.currency];
      const price = this.state.price / rate;
      callBack(price);
    } else {
      callBack(this.state.price);
    }
  };

  getLowestCategory = (categoryIds) => {
    let max = 0;
    let lowestCategory = {};

    categoryIds.forEach((categoryId) => {
      const {count, category} = this.countParents(categoryId);
      if (count > max){
        max = count;
        lowestCategory = category
      }
    });
    return lowestCategory;
  };

  countParents = (categoryId) => {
    let count = 0;
    let category = this.state.categoriesFlat.find(category => category.id === parseInt(categoryId));
    let parent = category.parent;
    while (parent !== null) {
      count++;
      parent = parent.parent
    }
    return {count, category};
  };

  submit = () => {
    this.setState({
      submitting: true
    }, () => {
      this.convertToBaseCurrency((price) => {
        const requestData = {
          product: {
            id: this.state.id,
            name: this.state.name,
            description: this.state.description,
            price: price
          },
          category: this.state.category,
          baseImage64: this.state.baseImage64,
          additionalImages64: this.state.additionalImages64
        };

        if (requestData.product.id !== null){
          //Update
          this.updateProduct(requestData);
        } else {
          //Add
          this.addProduct(requestData)
        }
      });
    });
  };

  updateProduct = (requestData) => {
    ProductsFrontendApi.updateProduct(requestData)
      .then(data => {
        this.afterSubmit();
      })
      .catch(error => {
        console.error(error);
        this.afterSubmit();
      });
  };

  addProduct = (requestData) => {
    ProductsFrontendApi.addProduct(requestData)
      .then(data => {
        this.afterSubmit();
      })
      .catch(error => {
        console.error(error);
        this.afterSubmit();
      })
  };

  afterSubmit = () => {
    this.setState({submitting: false})
    this.props.reloadData()
  };

  mapCategoriesToOptions = (categoryNode) => {
    let options = [];
    options.push(<option key={categoryNode.category.id} value={categoryNode.category.id}>{categoryNode.category.name}</option>);
    if (categoryNode.subCategories !== null && categoryNode.subCategories.length > 0){
      categoryNode.subCategories.forEach(subCategory => {
        options = options.concat(this.mapCategoriesToOptions(subCategory));
        return options;
      });
    }
    return options;
  };

  render() {
    const currencies = Object.keys(this.props.rates).map(currency =>
      <option key={currency} value={currency}>{currency}</option>
    );
    let cat = [];
    this.props.categories.forEach(categoryNode => {
      const options = this.mapCategoriesToOptions(categoryNode);
      cat = cat.concat(options);
    });

    return <Container>
      <Form>
        <Row>
          <Col xs={6}>
            <Form.Group controlId="form-product-name">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                name='name'
                onChange={this.onChange}
                placeholder="Name"
                value={this.state.name}
              />
            </Form.Group>
          </Col>
          <Col xs={6}>
            <Form.Group controlId="category.select">
              <Form.Label>Category</Form.Label>
              <Form.Control
                name='category'
                onChange={this.onChangeCategory}
                as="select"
                value={this.state.category.id}
              >
                {cat}
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Form.Group controlId="form-product-description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            name='description'
            onChange={this.onChange}
            placeholder="Please enter a description..."
            value={this.state.description} />
        </Form.Group>
        <Row>
          <Col className='product-price' xs={6}>
            <Form.Group controlId='product.price'>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type='number'
                name='price'
                onChange={this.onChange}
                value={this.state.price}
              />
            </Form.Group>
          </Col>
          <Col className='product-currency' xs={6}>
            <Form.Group controlId="currency.select">
              <Form.Label>Currency</Form.Label>
              <Form.Control
                name='currency'
                onChange={this.onChange}
                as="select"
                value={this.state.currency}
              >
                {currencies}
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col xs={12} md={6}>
            <div>Main Image</div>
            <FileBase64
              multiple={false}
              accept="*.png, *.jpg"
              onDone={this.handleBaseImage}
            />
          </Col>
          <Col xs={12} md={6}>
            <div>Additional Images</div>
            <FileBase64
              multiple={true}
              accept="*.png, *.jpg"
              onDone={this.handleAdditionalImages}
            />
          </Col>
        </Row>
        <Button
          className='product-submit-button'
          disabled={this.state.submitting}
          variant="primary"
          type="submit"
          onClick={this.submit}
        >
          Submit
        </Button>
      </Form>
    </Container>
  }

  onChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    })
  };

  onChangeCategory = (event) => {
    const value = event.target.value;
    const category = this.state.categoriesFlat.find(cat => cat.id === parseInt(value));
    if (category !== null){
      this.handleCategoryChanged(category)
    }
  };

  handleCategoryChanged(category){
    this.setState({category})
  }

  handleBaseImage = (baseImage) => {
    this.setState({baseImage64: baseImage.base64})
  };

  handleAdditionalImages = (additionalImages) => {
    const images = additionalImages.reduce((acc, current) => {
      acc.push(current.base64);
      return acc;
    }, []);
    this.setState({additionalImages64: images})
  };
}