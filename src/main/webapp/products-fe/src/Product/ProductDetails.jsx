import React, {Component} from 'react'
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import './ProductDetails.scss';
import ProductsFrontendApi from "../API/ProductsFrontendApi";

const IMG_BASE_PATH=process.env.REACT_APP_IMG_BASE_URL;

export default class ProductDetails extends Component {

  constructor(props) {
    super(props);
    this.state={
      id: "",
      name: "",
      description: "",
      price: "",
      currency: "EUR",
      baseImagePath: {
        id: "",
        path: ""
      },
      additionalImagePaths: []
    };
  }

  componentDidMount() {
    ProductsFrontendApi.getProduct(this.props.match.params.productId)
      .then(data => {
        this.setState({...data.data});
      })
      .catch(error => {
        console.error(error);
        this.setState({error: true});
      });
  }

  getPrice = (price) => {
    if (this.state.currency === "EUR"){
      return parseFloat(price).toFixed(2);
    }
    return (price * (this.props.rates[this.state.currency] || 1)).toFixed(2)
  };

  setAsMainImage = (additionalImageIndex) => {
    const mainImage = Object.assign({}, this.state.baseImagePath);
    const additionalImages = [...this.state.additionalImagePaths];

    const newMainImage = additionalImages.splice(additionalImageIndex, 1)[0];
    additionalImages.splice(additionalImageIndex, 0, mainImage);

    this.setState({
      baseImagePath: newMainImage,
      additionalImagePaths: additionalImages
    })
  };

  deleteProduct = () => {

    ProductsFrontendApi.deleteProduct(this.state.id)
      .then(data => {
        //TODO Toast
        this.props.reloadData();
        this.props.history.push(`/`);
      }).catch(error => console.error(error))
  };

  render() {

    const currencies = Object.keys(this.props.rates).map(currency =>
      <option key={currency} value={currency}>{currency}</option>
    );


    const additionalImages = this.state.additionalImagePaths.map((image, i) =>
      <img key={i} onClick={() => this.setAsMainImage(i)} className='product-additional-image' src={IMG_BASE_PATH + image.path} alt=""/>
    );
    return (
      <Container className='product-container'>
        <Row className='product-details-main-row'>
          <Col xs={12} md={4}>
            {this.state.baseImagePath.path &&
            <img className='product-main-image' src={IMG_BASE_PATH + this.state.baseImagePath.path} alt=""/>
            }

          </Col>
          <Col className='product-side-images' xs={12} md={2}>
            {additionalImages}
          </Col>
          <Col xs={12} md={4}>
            <Row className="product-name">
              <h1>{this.state.name}</h1>
            </Row>
            <Row>
              <Col className='product-price' xs={6}>
                {this.getPrice(this.state.price)}
              </Col>
              <Col className='product-currency' xs={6}>
                <Form.Group controlId="currency.select">
                  <Form.Label>Currency</Form.Label>
                  <Form.Control defaultValue='EUR' name='currency' onChange={this.onChange} as="select">
                    {currencies}
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
          </Col>
        </Row>
        <hr/>
        <Row className='product-details-description-row'>
          <Col>
            <p className='product-description'>
              {this.state.description}
            </p>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Button
            className='product-edit-button'
            variant="primary"
            onClick={() => this.props.history.push(`/products/edit/${this.state.id}`)}
          >
            Edit
          </Button>
          <Button
            className='product-edit-button'
            variant="danger"
            onClick={this.deleteProduct}
          >
            Delete
          </Button>
        </Row>
      </Container>
    )
  }

  onChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    })
  }
}