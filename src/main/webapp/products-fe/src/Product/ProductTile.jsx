import React from 'react'
import {Col} from "react-bootstrap";
import './ProductTile.scss'
import {Link} from "react-router-dom";

const IMG_BASE_PATH=process.env.REACT_APP_IMG_BASE_URL;

export default function ProductTile(props) {
  return (
    <Col md={3} className='product-tile-container'>
      <Link to={`/products/${props.id}`}>
        <div className="product-tile">
          <img className="product-tile-image" src={IMG_BASE_PATH + props.baseImagePath.path} alt={props.name}/>
          <div className='product-tile-banner'>
            <span className="product-tile-name">
              {props.name}
            </span>
          </div>
        </div>
      </Link>
    </Col>
  )
}