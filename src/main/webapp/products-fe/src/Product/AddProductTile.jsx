import React from 'react'
import {Link} from "react-router-dom";
import {Col} from "react-bootstrap";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

export default function AddProductTile(props) {

  return (
    <Col md={3} className='product-tile-container'>
      <Link to={`/products/add`}>
        <div className="product-tile add">
          <FontAwesomeIcon size="7x" icon="plus-square" />
        </div>
      </Link>
    </Col>
  )
}