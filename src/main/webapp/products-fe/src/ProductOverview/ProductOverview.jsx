import React, {Component} from 'react'
import ProductsFrontendApi from "../API/ProductsFrontendApi";
import ProductTile from "../Product/ProductTile";
import {Container, Row} from "react-bootstrap";
import './ProductOverview.scss'
import AddProductTile from "../Product/AddProductTile";

export default class ProductOverview extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    ProductsFrontendApi.getAllProducts().then((data) => {
      console.log(data.data);
      this.setState({
        products: data.data
      })
    })
  }

  render() {
    const productTiles = this.props.products
      .filter(this.categoryFilter)
      .map(product => <ProductTile key={product.id} {...product} />);
    productTiles.push(<AddProductTile key='add_product' />)



    return (
      <Container className='products-container'>
        <Row>
        </Row>
        <Row>
          {productTiles}
        </Row>
      </Container>
    )
  }

  categoryFilter = (product) => {
    if (this.props.match.params.categoryId){
      console.log(`${this.props.match.params.categoryId} - ${product.categoryIds}`);
      return product.categoryIds.includes(parseInt(this.props.match.params.categoryId));
    }
    return true;
  }
}