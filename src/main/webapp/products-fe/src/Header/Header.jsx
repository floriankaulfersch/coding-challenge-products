import React from 'react'
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import logo from '../img/dummy-logo.png'
import CategoryDropdown from "./CategoryDropdown";

import './Header.scss';
import {Link} from "react-router-dom";


export default function Header(props) {

  const categories = props.categories.map((category, i) => {
    return <CategoryDropdown
      key={category.category.name}
      {...category}
    />
  });
  /*categories.push(
    <Col key='add_category' xs={6} md={2}>
      <Link to="/categories/add" className='category-modfify-link'>Add</Link>
    </Col>
  );
  categories.push(
    <Col key='modify_category' xs={6} md={2}>
      <Link to="/categories" className='category-modfify-link'>Configure</Link>
    </Col>);*/

  return (
    <div className='header'>
      <Container>
        <Row>
          <Col xs={12} md={4}>
            <Link to="/">
              <img src={logo} alt="Logo" />
            </Link>

          </Col>
          <Col xs={12} md={8} className='heading'>
            <h1>Products</h1>
          </Col>
        </Row>
        <Row className='categories-row'>
          {categories}
        </Row>
      </Container>
    </div>

  )
}