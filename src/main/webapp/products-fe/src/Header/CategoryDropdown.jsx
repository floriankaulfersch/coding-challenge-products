import React from 'react'
import {Col, DropdownButton} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function CategoryDropdown(props) {

  const subCategories=mapSubCategories(props.subCategories);

  return (
    <Col xs={6} md={2}>
      <DropdownButton
        drop='down'
        variant='secondary'
        title={props.category.name}
        id={`category-${props.category.name}-dropdown-${props.category.id}`}
      >
        {subCategories}
      </DropdownButton>
    </Col>

  )
}

export function SubCategory(props){

  const subCategories = mapSubCategories(props.subCategories);

  return (
    <ul>
      <li><Link className="category-link" to={"/categories/"+props.category.id+"/products"}>
        {props.category.name}
      </Link></li>

      {subCategories}
    </ul>
  )
}

function mapSubCategories(subCategories){
  return subCategories ? subCategories.map(subCategory => <SubCategory key={subCategory.category.id} {...subCategory}/>) : [];
}