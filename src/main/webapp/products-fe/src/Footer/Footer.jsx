import React from 'react'
import {Col, Container, Row} from "react-bootstrap";
import './Footer.scss'

export default function Footer(props) {
  return (
    <Container className="footer">
      <Row>
        <Col xs={4} className='left-footer'>
          Florian Kaulfersch
        </Col>
        <Col xs={8} className='right-footer'>
          For HSE24
        </Col>
      </Row>
    </Container>
  )
}