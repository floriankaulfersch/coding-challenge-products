import axios from 'axios'

export default class FixerApi {

  static API_KEY = process.env.REACT_APP_FIXER_API_KEY;

  static getExchangeRates(base = 'EUR'){
    return axios.get(`http://data.fixer.io/api/latest?access_key=${FixerApi.API_KEY}&base=${base}`)
  }
}