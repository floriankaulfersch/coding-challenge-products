import axios from './AxiosConfig'

export default class ProductsFrontendApi {


  static getAllProducts = () => {
    return axios.get("/products");
  };

  static getProduct = (id) => {
    return axios.get(`/products/${id}`)
  };

  static addProduct = (productRequest) => {
    return axios.post("/products", productRequest)
  };

  static updateProduct = (productRequest) => {
    return axios.put(`/products/${productRequest.product.id}`, productRequest);
  };

  static deleteProduct = (productId) => {
    return axios.delete(`/products/${productId}`);
  };

  static getAllCategories = () => {
    return axios.get("/categories");
  };

  static getAllCategoriesFlat = () => {
    return axios.get("/categories/flat")
  };


}